class AppDelegate
  include MapKit
  include CoreLocation::DataTypes

  def application(application, didFinishLaunchingWithOptions:launchOptions)
    # rootViewController = UIViewController.alloc.init
    # rootViewController.title = 'ShareMapsExample'
    # rootViewController.view.backgroundColor = UIColor.whiteColor

    # navigationController = UINavigationController.alloc.initWithRootViewController(rootViewController)

    placemark = MKPlacemark.alloc.initWithCoordinate(CoordinateSpan.new(41.89328095359335, -87.67741452902555).api, addressDictionary:nil)
    map_item = MKMapItem.alloc.initWithPlacemark(placemark)
    map_item.setName('some address')

    # # Pass the map item to the Maps app
    map_item.openInMapsWithLaunchOptions(nil)


    # @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    # @window.rootViewController = navigationController
    # @window.makeKeyAndVisible

    true
  end
end
