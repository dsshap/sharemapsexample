class ShareViewController < SLComposeServiceViewController

  def viewDidLoad
    super
    if (item = extensionContext.inputItems.first).is_a?(NSExtensionItem)
      # NSLog("has NSExtensionItem")
      item.attachments.each_with_index do |attachment, index|
        # NSLog("index: #{index}")
        if attachment.hasItemConformingToTypeIdentifier("public.url")
          attachment.loadItemForTypeIdentifier("public.url", options: nil, completionHandler: proc{|url, err|
            if err.nil?
              if url.is_a?(NSURL)
                NSLog(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                NSLog("GOT URL (absoluteString): #{url.absoluteString}")
                NSLog(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
              else
                NSLog("ERROROORRRRRRRRRRRRRRRRR***************** Did not get NSURL")
              end
            else
              NSLog("ERROROORRRRRRRRRRRRRRRRR***************** loadItemForTypeIdentifier public.url")
            end
          })
        else
          # NSLog("ERROROORRRRRRRRRRRRRRRRR***************** hasItemConformingToTypeIdentifier public.url")
        end
      end
    else
      NSLog("ERROROORRRRRRRRRRRRRRRRR***************** no NSExtensionItem")
    end
  end

  def isContentValid
    # Do validation of contentText and/or NSExtensionContext attachments here
    true
  end

  def didSelectPost
    # This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.

    # Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    self.extensionContext.completeRequestReturningItems(nil, completionHandler:nil)
  end

  def configurationItems
    # To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    []
  end

end
